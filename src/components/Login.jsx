import React from 'react';
import axios from 'axios';

const DOMEN_SERVER = "---";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
        this.changeInputRegister = this.changeInputRegister.bind(this);
        this.submitChackin = this.submitChackin.bind(this);
    }

    changeInputRegister(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    submitChackin(event) {
        event.preventDefault();

        axios.post(DOMEN_SERVER + "/auth/login/", {
            username: this.state.username,
            password: this.state.password,
        }).then(res => {
            if (res.data === true) {
                alert("There is already a user with this email")
            } else {
                alert("Failed to login")
            }
        }).catch(() => {
            alert("Failed to login")
        })
    }

    render() {
        return (
            <div className="form">
                <h2>Register user:</h2>
                <form onSubmit={this.submitChackin}>
                    <p>Name: <input
                        type="username"
                        id="username"
                        name="username"
                        value={this.state.username}
                        onChange={this.changeInputRegister}
                    /></p>
                    <p>Password: <input
                        type="password"
                        id="password"
                        name="password"
                        value={this.state.password}
                        onChange={this.changeInputRegister}
                    /></p>
                    <input type="submit" />
                </form>
            </div>
        );
    }
}

export default Login;