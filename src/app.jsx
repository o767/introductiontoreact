import ReactDOM from 'react-dom';
import React from 'react';
import Login from './components/Login.jsx';

ReactDOM.render(
    <Login />,
    document.getElementById("app")
)